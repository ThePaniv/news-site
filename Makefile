news:
	docker-compose build
	docker-compose up # for production deployment, I should add -d (run in detached mode), but for reviewer convenience(to see logs) I had to remove it

clean_env:
	docker stop $$(docker ps -a -q)
	docker rm $$(docker ps -a -q)

restart: clean_env news
