# About
Simple news site API based on Django REST Framework.

# Project tech stack
* Python 3.7.4
* Django 2.2.6
* Django REST Framework 3.10.3
* Gunicorn 19.6.0
* Nginx 1.17.5
* Postgres 12.0
* Celery 4.3.0
* Redis 5.0.6
* Coverage 4.5.4
* Pylint 2.4.2
* Poetry ^0.2.17
* DRF-Ynsg 1.17.0
* Docker 19.03.4
* Docker-compose 1.8.0

# How to use
To set up project you only need to install Docker from this huge list. If Docker is not installed
yet, please, visit official Docker installation site and come back - https://docs.docker.com/v17.12/install/#server

### How to run docker
Project is based on 5 docker container, to build and run them all you need to execute only one command in parent project directory
```bash
make news
```
First build may take some time, as dockers need to pull some images from Docker hub and install 
project requirements. Please, be patient!

After few minutes you should see how news-app docker complete 3 basic CI/CD flows:
* check code quality with pylint
* run unit-tests with 100% coverage
* start Postgres, Nginx and gunicorn servers with DRF application

Pylint expected response:
```bash
news-app    | Your code has been rated at 10.00/10
```

Unit-tests expected response:
```bash
news-app    | Creating test database for alias 'default'...
news-app    | Ran 12 tests in 0.477s
news-app    | 
news-app    | OK
news-app    | Destroying test database for alias 'default'...
news-app    | Name                  Stmts   Miss  Cover   Missing
news-app    | ---------------------------------------------------
news-app    | __init__.py               0      0   100%
news-app    | main/__init__.py          0      0   100%
news-app    | main/admin.py            13      0   100%
news-app    | main/apps.py              5      0   100%
news-app    | main/models.py           24      0   100%
news-app    | main/permissions.py       7      0   100%
news-app    | main/serializers.py      29      0   100%
news-app    | main/tests.py            56      0   100%
news-app    | main/urls.py              8      0   100%
news-app    | main/views.py            25      0   100%
news-app    | news/__init__.py          2      0   100%
news-app    | news/celery.py            6      0   100%
news-app    | news/settings.py         48      0   100%
news-app    | news/urls.py              4      0   100%
news-app    | ---------------------------------------------------
news-app    | TOTAL                   227      0   100%
```

Deploy expected response:
```bash
news-app    | [2019-11-18 00:30:32 +0000] [23] [INFO] Starting gunicorn 19.6.0
news-app    | [2019-11-18 00:30:32 +0000] [23] [INFO] Listening at: http://0.0.0.0:8000 (23)
news-app    | [2019-11-18 00:30:32 +0000] [23] [INFO] Using worker: sync
news-app    | [2019-11-18 00:30:32 +0000] [26] [INFO] Booting worker with pid: 26

```

Also, you may notice something like that:
```bash
news-app    | Superuser created successfully.
```
Yes, Docker is creating default admin user. It was made for reviewer convenience. Of course,
I understand that it is not the best practice for real-life projects, but, to my mind, is suitable
for POC project like this. Email: blank@email.com, password: admin123!

Now you can visit __http://localhost:8000/news/posts/__ to create some posts.

Post object CRUD behaviour:
* Everyone can create new posts
* If you will create post as admin/staff user, it will be automatically validated and posted on the site
* If you will create post as normal user, you should wait until admin will validate it
* You can update and partial_update post only if you are post owner or admin

Comment object CRUD behaviour:
* Everyone can create new comments
* You can create comments only for post which are valid
* You can update and partial_update comments only if you are comment owner or admin

User object CRUD behaviour:
* Everyone can create new user
* Only user himself and admin can make next actions - retrieve, update and partial_update user
* Only admin can destroy user and get list of all users

Admin additional possibilities:
* Admin can edit user posts and comments
* Admin can change users role
* Admin can publish/unpublish posts (just click __is_valid__ radio button on Django admin page to publish post or unclick for opposite action)

# Swagger
Of course, for better reviewing experience and for convenience of possible FE engineer, this project has Swagger UI

You are welcome - http://localhost:8000/swagger/ 
