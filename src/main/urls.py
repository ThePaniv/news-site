"""
Main app routers registration
"""
from django.conf.urls import include, re_path
from rest_framework import routers
from main import views

router = routers.DefaultRouter()
router.register(r'posts', views.PostViewSet)
router.register(r'comments', views.CommentsViewSet)
router.register(r'users', views.UserViewSet)

urlpatterns = [
    re_path(r'^', include(router.urls))
]
