"""
Main app views module
"""
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny


from main.models import User, Post, Comment
from main.serializers import UserSerializer, PostSerializer, CommentsSerializer
from main.permissions import IsHimselfOrAdmin, IsAuthorOfAdmin


class BaseView(viewsets.ModelViewSet):
    """
    Base viewsets for global get_permissions method
    P.S. I hate code repetitions
    """
    permission_scopes = []

    def get_permissions(self):
        """
        Method to switch permissions based on HTTP action
        :return: permissions list
        """
        for scope in self.permission_scopes:
            if self.action in next(iter(scope)).split(','):
                return [next(iter(scope.values()))()]
        return [IsAdminUser()]


class UserViewSet(BaseView):
    """
    Viewset to CRUD custom users
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_scopes = [
        {'create': AllowAny},
        {'retrieve,update,partial_update': IsHimselfOrAdmin}
    ]


class PostViewSet(BaseView):
    """
    Viewset to CRUD Posts
    """
    queryset = Post.objects.filter(is_valid=True)
    serializer_class = PostSerializer
    permission_scopes = [
        {'create,list,retrieve': IsAuthenticated},
        {'update,partial_update': IsAuthorOfAdmin}
    ]


class CommentsViewSet(BaseView):
    """
    Viewset to CRUD Comments
    """
    queryset = Comment.objects.all()
    serializer_class = CommentsSerializer
    permission_classes = [IsAuthenticated]
    permission_scopes = [
        {'create,list,retrieve': IsAuthenticated},
        {'update,partial_update': IsAuthorOfAdmin}
    ]
