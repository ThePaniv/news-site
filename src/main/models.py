"""
Django ORM models module
"""
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from ckeditor_uploader.fields import RichTextUploadingField


class User(AbstractUser):
    """
    Custom Auth user model
    """
    username = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(_('email address'), unique=True)
    date_of_birth = models.DateField(blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        """
        Method to return more convenient model object representation
        :return: User email string
        """
        return "{}".format(self.email)


class Post(models.Model):
    """
    Post object model
    """
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    content = RichTextUploadingField()
    is_valid = models.BooleanField(default=False)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        """
        Method to return more convenient model object representation
        :return: Post basic info string
        """
        return f"Author: {self.author.email}, " \
            f"Title: {self.title}, " \
            f"Created: {self.created_at}"

    @property
    def author_email(self):
        """
        Lazy author_email Post property not stored in DB
        :return: Post author email string
        """
        return self.author.email


class Comment(models.Model):
    """
    Comments object model
    """
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    content = RichTextUploadingField()
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        """
        Method to return more convenient model object representation
        :return: Comment basic info string
        """
        return f"Post title: {self.post.title}, " \
            f"Comments content: {self.content}, " \
            f"Author: {self.author.email}, " \
            f"Created: {self.created_at}"
