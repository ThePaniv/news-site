"""
Model objects serializers module
"""
from rest_framework import serializers
from main.models import User, Post, Comment


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Class to serializer custom User model
    """

    class Meta:
        """
        UserSerializer class for required configurations
        """
        model = User
        fields = ['id', 'url', 'email', 'first_name', 'last_name', 'password', 'date_of_birth']
        extra_kwargs = {'password': {'write_only': True}}


class CommentsSerializer(serializers.ModelSerializer):
    """
    Class to serializer Comment model
    """
    class Meta:
        """
        CommentsSerializer class for required configurations
        """
        model = Comment
        fields = ['id', 'url', 'author', 'post', 'content', 'created_at']
        extra_kwargs = {'author': {'read_only': True}}

    def create(self, validated_data):
        """
        Method to override ModelSerializer create method, so I can set author to currently logged in user
        :param validated_data: validated data to work with
        :return: new Comment object
        """
        post = validated_data.get('post')
        if post.is_valid:
            comment = Comment.objects.create(author=self.context['request'].user, **validated_data)
            comment.save()
            return comment
        raise serializers.ValidationError(f'Post {post.id} is not valid for commenting')


class PostSerializer(serializers.ModelSerializer):
    """
    Class to serialize Post model
    """
    comments = CommentsSerializer(serializers.PrimaryKeyRelatedField(queryset=Comment.objects.all()),
                                  many=True, read_only=True)

    class Meta:
        """
        PostSerializer class for required configurations
        """
        model = Post
        fields = ['id', 'url', 'author_email', 'title', 'content', 'created_at', 'comments']
        extra_kwargs = {'author_email': {'read_only': True}}

    def create(self, validated_data):
        """
        Method to override ModelSerializer create method, so I can validate posts if request.user
        is admin or staff(editor)
        :param validated_data: validated data to work with
        :return: new Post object
        """
        current_user = self.context['request'].user
        post = Post.objects.create(author=current_user, **validated_data)

        if current_user.is_superuser or current_user.is_staff:
            post.is_valid = True

        post.save()
        return post
