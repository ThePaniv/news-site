"""
Custom permissions module
"""
from rest_framework.permissions import BasePermission


class IsHimselfOrAdmin(BasePermission):
    """
    Custom permission to check if request.user want to make retrieve/update/partial_update action
    on his own user or request.user is admin
    """

    def has_object_permission(self, request, view, obj):
        """
        Method to check if request.user has a permission to requested object
        :param request: django request object
        :param view: django requested view
        :param obj: requested object
        :return: boolean to give permissions or not
        """
        return obj == request.user or request.user.is_superuser


class IsAuthorOfAdmin(BasePermission):
    """
    Custom permission to check if request.user want to make update/partial_update action on his own
    post/comment ot request.user is admin
    """

    def has_object_permission(self, request, view, obj):
        """
        Method to check if request.user has a permission to requested object
        :param request: django request object
        :param view: django requested view
        :param obj: requested object
        :return: boolean to give permissions or not
        """
        return obj.author == request.user or request.user.is_superuser
