"""
Django unit-tests module
"""
from rest_framework.test import APIClient, APITestCase
from main.models import Post, Comment, User


class MainTestCase(APITestCase):
    """
    Test class for main app
    """

    def setUp(self):
        """
        Method to be executed before every test, so it can create test user, admin and post
        to work with
        :return:
        """
        self.email = 'test@gmail.com'
        self.password = 'test_password'
        self.user = User.objects.create(email=self.email,
                                        password=self.password)
        self.user_client = APIClient(enforce_csrf_checks=True)
        self.user_client.force_authenticate(user=self.user)

        self.admin = User.objects.create(email=self.email[1:],
                                         password=self.password,
                                         is_superuser=True)
        self.admin_client = APIClient(enforce_csrf_checks=True)
        self.admin_client.force_authenticate(user=self.admin)

        self.user_post = Post.objects.create(author=self.user,
                                             title='TestTitle',
                                             content='TestContent',
                                             is_valid=True)
        self.user_comment = Comment.objects.create(author=self.user,
                                                   content='TestContent',
                                                   post=self.user_post)
        self.admin_post = Post.objects.create(author=self.admin,
                                              title='TestTitle',
                                              content='TestContent',
                                              is_valid=True)
        self.admin_comment = Comment.objects.create(author=self.admin,
                                                    content='TestContent',
                                                    post=self.admin_post)

    def test_valid_post_creation(self):
        """
        Test valid post object creation
        :return:
        """
        response = self.user_client.post('/news/posts/',
                                         {'title': 'TestTitle', 'content': 'TestContent'})
        self.assertEqual(response.status_code, 201)

        response = self.user_client.get(f'/news/post/{response.data["id"]}/')
        self.assertEqual(response.status_code, 404)

    def test_valid_admin_post_creation(self):
        """
        Test valid post creation by Admin
        :return:
        """
        response = self.admin_client.post('/news/posts/',
                                          {'title': 'TestTitle', 'content': 'TestContent'})
        self.assertEqual(response.status_code, 201)

        response = self.admin_client.get(f'/news/posts/{response.data["id"]}/')
        self.assertEqual(response.status_code, 200)

    def test_valid_comment_creation(self):
        """
        Test valid comment creation
        :return:
        """
        response = self.user_client.post('/news/comments/',
                                         {'post': self.user_post.id, 'content': 'TestContent'})
        self.assertEqual(response.status_code, 201)

    def test_valid_user_creation(self):
        """
        Test valid new user creation
        :return:
        """
        response = self.user_client.post('/news/users/',
                                         {'username': 'test_username',
                                          'email': 'test2@gmail.com',
                                          'password': 'test_password',
                                          'password2': 'test_password'})
        self.assertEqual(response.status_code, 201)

    def test_valid_user_update(self):
        """
        Test valid user update
        :return:
        """
        response = self.user_client.put(f'/news/users/{self.user.id}/',
                                        {'last_name': 'test_last_name',
                                         'email': self.email, 'password': self.password})
        self.assertEqual(response.status_code, 200)

    def test_valid_post_update(self):
        """
        Test valid post update
        :return:
        """
        response = self.user_client.put(f'/news/posts/{self.user_post.id}/',
                                        {'title': 'new_test_title',
                                         'content': 'new_test_content'})
        self.assertEqual(response.status_code, 200)

    def test_valid_comment_update(self):
        """
        Test valid comment update
        :return:
        """
        response = self.user_client.put(f'/news/comments/{self.user_comment.id}/',
                                        {'content': 'new_test_content',
                                         'post': self.user_post.id})
        self.assertEqual(response.status_code, 200)

    def test_invalid_comment_create(self):
        """
        Test commenting not validated post
        :return:
        """
        response = self.user_client.post('/news/posts/',
                                         {'title': 'TestTitle', 'content': 'TestContent'})
        self.assertEqual(response.status_code, 201)
        response = self.user_client.post(f'/news/comments/',
                                         {'post': response.data['id'], 'content': 'TestContent'})
        self.assertEqual(response.status_code, 400)

    def test_forbidden_post_update(self):
        """
        Test another user post update
        :return:
        """
        response = self.user_client.put(f'/news/posts/{self.admin_post.id}/')
        self.assertEqual(response.status_code, 403)

    def test_forbidden_comment_update(self):
        """
        Test another user comment update
        :return:
        """
        response = self.user_client.put(f'/news/comments/{self.admin_comment.id}/')
        self.assertEqual(response.status_code, 403)

    def test_forbidden_user_update(self):
        """
        Test another user update
        :return:
        """
        response = self.user_client.put(f'/news/users/{self.admin.id}/',
                                        {'last_name': 'test_last_name'})
        self.assertEqual(response.status_code, 403)

    def test_forbidden_user_retrieve(self):
        """
        Test another user retrieve
        :return:
        """
        response = self.user_client.get(f'/news/users/{self.admin.id}/')
        self.assertEqual(response.status_code, 403)

    def test_forbidden_user_destroy(self):
        """
        Test another user destroy
        :return:
        """
        response = self.user_client.delete(f'/news/users/{self.admin.id}/')
        self.assertEqual(response.status_code, 403)
