"""
Django signals module
"""
import sys
from django.db.models.signals import post_save
from django.dispatch import receiver

from main.models import Comment
from main.tasks import send_comment_notification_email_task


@receiver(post_save, sender=Comment)
def sent_comment_creation_email(sender, **kwargs):
    """
    Django signal to trigger celery task to send email about new comment
    :param sender: signal sender
    :param kwargs: dict to work with
    :return:
    """
    if kwargs['created']:
        comment = kwargs['instance']
        message = 'Hi!<br>' + \
                  f'Your post was commented by {comment.author.email}!<br>' + \
                  f'Commented at: {comment.created_at}<br>' + \
                  f'Comment content: {comment.content}'
        if 'test' not in sys.argv:
            send_comment_notification_email_task.delay('Post was commented',
                                                       message,
                                                       comment.post.author.email)
