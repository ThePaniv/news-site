"""
Main app configuration module
"""
from django.apps import AppConfig


class MainConfig(AppConfig):
    """
    Main app configuration class
    """
    name = 'main'

    def ready(self):
        """
        Method to be executed after MainApp is initialized, so i can import my Django signals
        :return:
        """
        from main import signals
