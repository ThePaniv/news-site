"""
Celery tasks module
"""
from celery import shared_task

from django.core.mail import EmailMessage
from django.conf import settings


@shared_task
def send_comment_notification_email_task(subject, message, recipient):
    """
    Celery task to send email about new comment
    :param subject: email subject
    :param message: email content
    :param recipient: email recipient
    :return:
    """
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_MAIL, [recipient])
    msg.content_subtype = "html"
    msg.send()
