"""
Custom createsuperuser command module
"""
from django.contrib.auth.management.commands import createsuperuser
from django.core.management import CommandError

from main.models import User


class Command(createsuperuser.Command):
    """
    Custom command class
    """
    help = 'Crate a superuser, and allow password to be provided'

    def add_arguments(self, parser) -> None:
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '--password', dest='password', default=None,
            help='Specifies the password for the superuser.',
        )

    def handle(self, *args: tuple, **options: dict) -> None:
        password = options.get('password')
        username = options.get('username')
        database = options.get('database')
        if not User.objects.filter(username=username).exists():

            if password and not username:
                raise CommandError("--username is required if specifying --password")

            super(Command, self).handle(*args, **options)

            if password:
                user = self.UserModel._default_manager.db_manager(database).get(username=username)  # pylint: disable=protected-access
                user.set_password(password)
                user.save()
