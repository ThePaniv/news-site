#!/usr/bin/env bash
pylint src
python manage.py makemigrations
python manage.py migrate
coverage run --source='.' manage.py test
coverage report --fail-under=100
python manage.py my_createsuperuser --username admin --password admin123 --email 'blank@email.com' --noinput
python manage.py collectstatic --no-input
gunicorn news.wsgi -b 0.0.0.0:8000
