"""
news URL Configuration module
"""
from django.contrib import admin
from django.urls import re_path
from django.urls import include
from news.swagger import schema_view

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^news/', include('main.urls')),
    re_path(r'^accounts/', include('allauth.urls')),
    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui')
]
